$( document ).ready(function() {
	

	
	// init Isotope
	/*var $grid = $('.grid').isotope({
	  itemSelector: '.article',
	  layoutMode: 'fitRows'
	});*/
	
	$('.grid').isotope({
	  itemSelector: '.article',
	  layoutMode: 'fitRows'
	});
	
	// filter functions
	var filterFns = {};
	
	// bind filter button click
	$('.filters').on( 'click', 'button', function() {
	  	var filterValue = $( this ).attr('data-filter');
	  	// use filterFn if matches value
	  	filterValue = filterFns[ filterValue ] || filterValue;
	  	//$grid.isotope({ filter: filterValue });
  		$(this).closest('.news').find('.grid').isotope({ filter: filterValue });
	});
	

	// change is-checked class on buttons
	$('.button-group').each( function( i, buttonGroup ) {
	  var $buttonGroup = $( buttonGroup );
	  $buttonGroup.on( 'click', 'button', function() {
		$buttonGroup.find('.news-cat-active').removeClass('news-cat-active');
		$( this ).addClass('news-cat-active');
	  });
	});
	  
	
	
	
	
	
	/* Load News */
	
	$('.news').on('click', '.page-navigation a', function (e) {
		
		var ajaxUrl = $(this).data('link');
				
		if (ajaxUrl !== undefined && ajaxUrl !== '') {
			e.preventDefault();
			var container = 'news-container-' + $(this).data('container');
			
			$.ajax({
				url: ajaxUrl,
				type: 'GET',
				success: function (result) {
					var ajaxDom = $(result).find('#' + container).children();
					var nextPage = $(result).find('.pagination');
					//$('#' + container).replaceWith(ajaxDom);
					$('#'+container).closest('.news').find('.pagination').remove();
					$('#'+container+' .news-clear').remove();
					$('#'+container).closest('.news').find('.page-navigation').append(nextPage);
					
					//console.log(ajaxDom);
					
					$('#' + container).append(ajaxDom).isotope('insert', ajaxDom);
							
				}
			});
		
		}
	});

	
	
	
	
		
	
});
