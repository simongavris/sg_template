<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "rm_template"
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Rawk Template',
    'description' => 'Basis Template',
    'category' => 'frontend',
    'author' => 'Chris Rawk',
    'author_email' => 'ce@rawk.at',
    'state' => 'alpha',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'dividers2tabs' => true,
    'clearCacheOnLoad' => 1,
    'version' => '0.6.2',
    'constraints' => array(
        'depends' => array(
            'typo3' => '8.0.0-8.9.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
