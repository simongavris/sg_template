<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}


// $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'][] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Classes/Hooks/HookExtTablesPostProcessing.php:HookExtTablesPostProcessing';



if (TYPO3_MODE === 'BE') {
 
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rm_template/Configuration/UserTS/UserTSconfigFile.t3s">'
    );
}


// Add BackendLayouts BackendLayouts for the BackendLayout DataProvider
//if (!$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]['disablePageTsBackendLayouts']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/Mod/WebLayout/BackendLayouts.txt">');
//}

call_user_func(
    function ($extConfString) {

        // register icons
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        $iconRegistry->registerIcon(
            'tx-rmtemplate-col2',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/gridlayout_col2.gif']
        );
        $iconRegistry->registerIcon(
            'tx-rmtemplate-col3',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/gridlayout_col3.gif']
        );
        $iconRegistry->registerIcon(
            'tx-rmtemplate-col4',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/gridlayout_col4.gif']
        );
		$iconRegistry->registerIcon(
            'tx-rmtemplate-col5',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/gridlayout_col5.gif']
        );
		$iconRegistry->registerIcon(
            'tx-rmtemplate-col6',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/gridlayout_col6.gif']
        );
        $iconRegistry->registerIcon(
            'tx-rmtemplate-simpletabs',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/grid_simpletabs.png']
        );
        $iconRegistry->registerIcon(
            'tx-rmtemplate-tabs4',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/grid_tabs4.png']
        );
        $iconRegistry->registerIcon(
            'tx-rmtemplate-tabs6',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/grid_tabs6.png']
        );
        $iconRegistry->registerIcon(
            'tx-rmtemplate-accordion',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/grid_accordion.png']
        );
        $iconRegistry->registerIcon(
            'tx-rmtemplate-slider',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/grid_slider.png']
        );
		$iconRegistry->registerIcon(
            'tx-rmtemplate-counter',
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:rm_template/Resources/Public/Icons/grid_counter.png']
        );

        // Add pageTS config
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rm_template/Configuration/TypoScript/tsconfig.ts">');

        // Get ext configuration
        strlen($extConfString)?$extConf = unserialize($extConfString):$extConf = array();

        // Only if enabled
        if ( isset($extConf['enableGridSimpleRow']) && $extConf['enableGridSimpleRow'] ) {
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rm_template/Configuration/TypoScript/simpleRow/tsconfig.ts">');
        }

    },$_EXTCONF
);
