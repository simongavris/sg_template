tx_rmtemplate {
    setup {
        # layout ID
        xSimpleRow {
            title = LLL:EXT:rm_template/Resources/Private/Language/locallang_db.xlf:xSimpleRow.title
            description = LLL:EXT:rm_template/Resources/Private/Language/locallang_db.xlf:xSimpleRow.description
            icon = EXT:rm_template/Resources/Public/Icons/gridlayout_row.gif
            frame = 1
            topLevelLayout = 0
            config {
                colCount = 1
                rowCount = 1
                rows.1.columns.1 {
					name = LLL:EXT:rm_template/Resources/Private/Language/locallang_db.xlf:celayout.columnElements
					colPos = 111
                }
            }
            #flexformDS = FILE:EXT:rm_template/Configuration/FlexForm/flexform_...
        }
    }
}